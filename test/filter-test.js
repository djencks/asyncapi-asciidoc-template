/* eslint-env mocha */
'use strict';

const { expect } = require('chai');

const all = require('../filters/all');

describe('escapeAttributes test', () => {
  it('curly brackets', () => {
    expect(all.escapeAttributes('foo {bar} baz')).to.equal('foo \\{bar} baz');
  });

  it('square brackets', () => {
    expect(all.escapeAttributes('foo [bar] baz')).to.equal('foo \\[bar] baz');
  });

  it('link', () => {
    expect(all.escapeAttributes('foo <<zbos/motion/animation/run>> baz')).to.equal('foo <<_zbosmotionanimationrun,zbos/motion/animation/run>> baz');
  });

  it('link with curly brackets', () => {
    expect(all.escapeAttributes('foo <<zbos/motion/animation/{key}/run>> baz')).to.equal('foo <<_zbosmotionanimationkeyrun,zbos/motion/animation/\\{key}/run>> baz');
  });

  it('link with target text', () => {
    expect(all.escapeAttributes('foo <<zbos/motion/animation/run,exciting channel>> baz')).to.equal('foo <<_zbosmotionanimationrun,exciting channel>> baz');
  });

  it('link with curly brackets with target text', () => {
    expect(all.escapeAttributes('foo <<zbos/motion/animation/{key}/run,exciting channel>> baz')).to.equal('foo <<_zbosmotionanimationkeyrun,exciting channel>> baz');
  });
});
