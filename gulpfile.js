'use strict'

const concat = require('gulp-concat')
const gulp = require('gulp')
const merge = require('merge-stream')
const nunjucks = require('gulp-nunjucks')
const filter = require('./filters/all')

exports.default = () => {
  let first = true

  const wrapper = function (templates, opts) {
    const out = first ? ['\'use strict\''] : ['']
    first = false
    templates.forEach(({ name, template }) => {
      out.push(`module.exports[\'${name}\'] = function () {
${template}
}()`)
    })
    return out.join('\n')
  }

  return merge(
    gulp.src('*/*.adoc')
      .pipe(nunjucks.precompile({ filter, wrapper }))
  )
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('dist'))
}
