'use strict';
const filters = require('./filters/all');
const templates = require('./dist/templates');
const PrecompiledLoader = require('./lib/precompiled-loader');
const loader = new PrecompiledLoader(templates);

module.exports = {
  filters,
  templates,
  loader,
};
