'use strict';
const { Loader } = require('nunjucks');
const { posix: path } = require('path');

//replaces resolve method with one that works with non-absolute 'from' paths
//Proposed to nunjucks as https://github.com/mozilla/nunjucks/issues/1326
class PrecompiledLoader extends Loader {
  constructor (compiledTemplates) {
    super();
    this.precompiled = compiledTemplates || {};
  }

  getSource (name) {
    if (this.precompiled[name]) {
      return {
        src: {
          type: 'code',
          obj: this.precompiled[name],
        },
        path: name,
      };
    }
    return null;
  }

  resolve (from, to) {
    return path.normalize(`${path.dirname(from)  }/${  to}`);
  }
}

module.exports = PrecompiledLoader;
