{% macro navChannels(prefix, outFilename, asyncapi, byTag) %}
{%- if byTag -%}

{%- for tagName, channelsForTag in asyncapi | byTags %}

** {{ tagName }}

{% for channelName, chan in channelsForTag -%}
*** xref:{{ prefix }}{{ outFilename }}#{{ channelName | toId }}[{{ channelName | escapeAttributes }}]
{% endfor -%}
{% endfor -%}

{%- else -%}

{% for channelName, chan in asyncapi.channels() %}
** xref:{{ prefix }}{{ outFilename }}#{{ channelName | toId }}[{{ channelName | escapeAttributes }}]
{%- endfor -%}

{%- endif -%}


{%- endmacro -%}
