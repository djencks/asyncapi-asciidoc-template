{% macro schemaTableRow(prop, propName, required=false, path='', circular=false) %}

| {%- if path | tree -%}[.asyncapi-property-path]#`{{ path | tree }}`#{% endif %}
[.asyncapi-property]#`{{ propName }}`# {% if required %}[.asyncapi-property-required]#*(required)*#{% endif %}

| [.asyncapi-property-type]#{{ prop.type() }}# {%- if prop.anyOf() -%}anyOf{%- endif -%}{%- if prop.allOf() -%}allOf{%- endif -%}{%- if prop.oneOf() %}oneOf{%- endif -%} {%- if prop.items().type %} [.asyncapi-property-items-type]#({{prop.items().type()}})#{%- endif %}

| {%- if prop.description() -%}[.asyncapi-property-description]#{{ prop.description() | safe | escapeAttributes }}#{% endif -%}
{% if circular %}[.asyncapi-property-circular]#*(circular)*#{% endif %}

| [.asyncapi-property-values]#{{ prop.enum() | acceptedValues | safe }}#
{%- endmacro -%}
